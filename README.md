# Status

This project has been abandoned. I am no longer on Roblox due to various circumstances. One of which being the fact
that Roblox is actively hostile towards its Linux userbase. If you want to use Roblox on Linux, please find another
solution.

If you want to fork this project, you will have to change the branding. This includes the name of the software and 
the branding/logo. Allowing people to publish new software under the same name has weird security and trust
relationship implications.

# Grapejuice

Roblox works quite well with Wine, though there are some minor issues running it. Grapejuice fills in the
gaps around these issues, so you can experience Roblox to its fullest on your favourite Linux distribution.

The primary gap-filler feature is the fact that Wine by default creates no protocol handlers, which is how Roblox
functions at its core. Without protocol handling, you won't be able to launch Roblox Studio and Experiences from the
website!

Note that Grapejuice is unofficial software. This is not officially supported by Roblox.

![Grapejuice main window](readme-images/grapejuice-main.png)

---

## Installing Grapejuice

The installation guides can be found in the [documentation](https://brinkervii.gitlab.io/grapejuice/). Please read
it thoroughly!

## Troubleshooting

Are you experiencing some trouble running Roblox Studio with Grapejuice? Please check out
the [troubleshooting guide](https://brinkervii.gitlab.io/grapejuice/reference/troubleshooting/).

## Features

- Manages multiple wineprefixes. Keep customizations for the Player and Studio separated.
- Edit Roblox experiences from the website
- Enjoy Roblox experiences by launching them from the website
- FFlag editor for tweaking Roblox's behaviour
- Builtin FPS unlocker
- Automatic graphics acceleration
- Support for managing DXVK

---

## Screenshots

### Graphics settings

![Graphics Settings](readme-images/settings-graphics.png)

### Third party integrations

![Integration Settings](readme-images/settings-integrations.png)

### Shortcuts to common actions

![Shortcuts](readme-images/settings-shortcuts.png)
